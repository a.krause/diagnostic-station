import java.util.concurrent.CountDownLatch;

public class Main {
    public final static CountDownLatch count = new CountDownLatch(2);

    public static void main(String[] args)throws InterruptedException {
        Thread thread1=new Thread(new Verification());
        Thread thread2=new Thread(new Diagnosis());
        thread1.start();
        thread2.start();
        count.await();
        System.err.println("TOTAL ERRORS : 2");
    }
}
//a.krause